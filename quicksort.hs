quickSort :: [Int] -> [Int]
quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs, y<=x ] ++ [x] ++ quickSort [y | y <- xs, y>x ]

-- algoritma dari quicksort yaitu kita pilih satu elemen x dari list, kemudian kita memecah list menjadi dua bagian
-- list pertama (sebelah kiri) berisi elemen elemen yang nilainya lebih kecil atau sama dengan elemen x
-- list kedua (sebelah kanan) berisi elemen elemen yang nilainya lebih besar dari elemen x
