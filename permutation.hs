permutation [] = [[]]
permutation ls = [x:sisa | x <- ls, sisa <- permutation(ls \\ [x])]