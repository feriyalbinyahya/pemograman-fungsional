primes = sieve [2..]
    where
        sieve (x:xs) = x : sieve [y | y <- xs, y `mod` x /= 0 ]
        
-- algoritmanya : mengambil elemen pertama x dari list untuk dijadikan 
