data Expr = C Float | Expr :+ Expr | Expr :- Expr
           | Expr :* Expr | Expr :/ Expr 
           | V [Char]
           | Let String Expr Expr      
      deriving Show
      
evaluate :: Expr -> Float
evaluate (C x)          = x
evaluate (e1 :+ e2)     = evaluate e1 + evaluate e2
evaluate (e1 :- e2)     = evaluate e1 - evaluate e2
evaluate (e1 :* e2)     = evaluate e1 * evaluate e2
evaluate (e1 :/ e2)     = evaluate e1 / evaluate e2
evaluate (Let v e0 e1)  = evaluate (subst v e0 e1)
evaluate (V _)          = 0.0

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1)             = if v0 == v1 then e0 else V v1
subst _ _ (C c)                = C c
subst v0 e0 (e1:+e2)           = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1:-e2)           = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1:*e2)           = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1:/e2)           = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2)     = Let v1 e1 (subst v0 e0 e2)

-- fungsi subst untuk mensubstitusi setiap variabel x yang ada di dalam ekspresi, dengan value dari variabel x yang sudah diberikan
-- fungsi subst rekursif karena didalam ekspresi bisa terdapat banyak ekspresi. Jadi pemecahan ekspresi tersebut menggunakan rekursif
-- hingga base nya adalah konstanta, atau sebuah variabel tunggal.

-- fungsi evaluate digunakan untuk komputasi matematika dari ekspresi setelah dilakukan subtitusi variabel dgn value.
-- fungsi evaluate bisa didefinisikan secara rekursif karena adanya pengulangan komputasi di dalam satu ekspresi.
-- hingga base nya adalah sebuah konstanta atau variabel yg tidak dapat di subtitusi sebelumnya.

-- Contoh evaluasi aritmatika tanpa variabel
-- >>> exp0 = (((C 4.0) :+ (C 5) :* (C 9)))
-- >>> evaluate exp0
-- evaluate (((C 4.0) :+ (C 5.0) :* (C 9.0))))
-- evaluate (C 4.0) + evaluate ((C 5.0) :* (C 9.0))
-- evaluate (C 4.0) + evaluate (C 5.0) * evaluate (C 9.0)
-- 4 + 5 + 9
-- 18





-- Latihan :
fold1 (tambah,kurang,kali,bagi) (C c) = c
fold1 (tambah,kurang,kali,bagi) (e1 :+ e2 ) = tambah (fold1 (tambah,kurang,kali,bagi) e1) (fold1 (tambah,kurang,kali,bagi) e2)
fold1 (tambah,kurang,kali,bagi) (e1 :- e2 ) = kurang (fold1 (tambah,kurang,kali,bagi) e1) (fold1 (tambah,kurang,kali,bagi) e2)
fold1 (tambah,kurang,kali,bagi) (e1 :* e2 ) = kali (fold1 (tambah,kurang,kali,bagi) e1) (fold1 (tambah,kurang,kali,bagi) e2)
fold1 (tambah,kurang,kali,bagi) (e1 :/ e2 ) = bagi (fold1 (tambah,kurang,kali,bagi) e1) (fold1 (tambah,kurang,kali,bagi) e2)

evaluate2 = fold1 (tambah1, kurang1, kali1, bagi1)
    where tambah1 = (+)
          kurang1 = (-)
          kali1 = (*)
          bagi1 = (/)

-- fungsi evaluate2 menerima input ekspresi :
-- evaluate2 expr = fold1 (tambah,kurang,kali,bagi) expr
